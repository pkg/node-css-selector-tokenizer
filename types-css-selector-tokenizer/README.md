# Installation
> `npm install --save @types/css-selector-tokenizer`

# Summary
This package contains type definitions for css-selector-tokenizer (https://github.com/css-modules/css-selector-tokenizer).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/css-selector-tokenizer.

### Additional Details
 * Last updated: Tue, 06 Jul 2021 20:32:34 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Avi Vahl](https://github.com/AviVahl).
